### see https://juliadynamics.github.io/Agents.jl/dev/examples/sir/ for details
### this code is lightly modified version of it

using LinearAlgebra
using Agents, Random, DataFrames, LightGraphs
using Distributions: Poisson, DiscreteNonParametric
using DrWatson: @dict

mutable struct PoorSoul <: AbstractAgent
  id::Int
  pos::Int
  days_infected::Int  # number of days since is infected
  status::Symbol  # 1: S, 2: I, 3:R
end

function model_initiation(;Ns, migration_rates, β_und, β_det, infection_period = 30,
  reinfection_probability = 0.05, detection_time = 14, death_rate = 0.02,
  Is=[zeros(Int, length(Ns)-1)..., 1], seed = 0)

  Random.seed!(seed)
  @assert length(Ns) == length(Is) == length(β_und) == length(β_det) == size(migration_rates, 1) "length of Ns, Is, and B, and number of rows/columns in migration_rates should be the same "
  @assert size(migration_rates, 1) == size(migration_rates, 2) "migration_rates rates should be a square matrix"

  C = length(Ns)
  # normalize migration_rates
  migration_rates_sum = sum(migration_rates, dims=2)
  for c in 1:C
    migration_rates[c, :] ./= migration_rates_sum[c]
  end

  properties =
    @dict(Ns, Is, β_und, β_det, β_det, migration_rates, infection_period,
    infection_period, reinfection_probability, detection_time, C, death_rate)
  space = Space(complete_digraph(C))
  model = ABM(PoorSoul, space; properties=properties)

  # Add initial individuals
  for city in 1:C, n in 1:Ns[city]
    ind = add_agent!(city, model, 0, :S) # Susceptible
  end
  # add infected individuals
  for city in 1:C
    inds = get_node_contents(city, model)
    for n in 1:Is[city]
      agent = id2agent(inds[n], model)
      agent.status = :I # Infected
      agent.days_infected = 1
    end
  end
  return model
end


function create_params(;Ns, max_travel_rate, infection_period = 30,
    reinfection_probability = 0.05, detection_time = 14, death_rate = 0.02,
    Is=[zeros(Int, length(Ns)-1)..., 1], seed = 19, improv = 100,
  )

  Random.seed!(seed)

  C = length(Ns)

  β_und = rand(0.03:0.02:0.2, C) # this gives a mean around 1.5 to 3 over 20 days
  β_det = β_und ./ improv # once detected, contagion rate goes down a lot

  Random.seed!(seed)
  migration_rates = zeros(C, C);
  for c in 1:C
    for c2 in 1:C
    migration_rates[c, c2] = (Ns[c] + Ns[c2])/Ns[c] # movement rates between cities is only function of population (we can do better)
    end
  end
  
  maxM = maximum(migration_rates)
  migration_rates = (migration_rates .* max_travel_rate) ./ maxM
  migration_rates[diagind(migration_rates)] .= 1.0

  params = @dict(Ns, β_und, β_det, migration_rates, infection_period,
    reinfection_probability, detection_time, death_rate, Is)
  return params
 end


function agent_step!(agent, model)
  migrate!(agent, model)
  transmit!(agent, model)
  update!(agent, model)
  recover_or_die!(agent, model)
end

function migrate!(agent, model)
  nodeid = agent.pos
  d = DiscreteNonParametric(1:model.properties[:C], model.properties[:migration_rates][nodeid, :])
  m = rand(d)
  if m ≠ nodeid
    move_agent!(agent, m, model)
  end
end

function transmit!(agent, model)
  agent.status == :S && return
  prop = model.properties
  rate = if agent.days_infected < prop[:detection_time]
    prop[:β_und][agent.pos]
  else
    prop[:β_det][agent.pos]
  end

  d = Poisson(rate)
  n = rand(d)
  n == 0 && return

  for contactID in get_node_contents(agent, model)
    contact = id2agent(contactID, model)
    if contact.status == :S || (contact.status == :R && rand() ≤ prop[:reinfection_probability])
      contact.status = :I
      n -= 1
      n == 0 && return
    end
  end
end

update!(agent, model) = agent.status == :I && (agent.days_infected += 1)

function recover_or_die!(agent, model)
  if agent.days_infected ≥ model.properties[:infection_period]
    if rand() ≤ model.properties[:death_rate]
      kill_agent!(agent, model)
    else
      agent.status = :R
      agent.days_infected = 0
    end
  end
end

infected(x) = count(i == :I for i in x)
recovered(x) = count(i == :R for i in x)

using DataFrames, CSV

cities = CSV.read("../../Data/cities/NZ_Urban_Area_Latitude_Longitude.csv") |> DataFrame

pops = parse.(Int,replace.(cities.Population, "," => ""))
pops = ceil.(pops/100)

params = create_params(Ns = pops, # number of agents in each city
		       infection_period = 20, # total length of infection (from infect to removed)
		       reinfection_probability = 0.0001, # probability of being reinfected
		       death_rate = 0.015, # probability of dying if infected
		       max_travel_rate=0.01, # scaling effect for travelling across regions
		       detection_time = 1) # how many days does it take to be detected?

model = model_initiation(;params...)

data_to_collect = Dict(:status => [infected, recovered, length]);
data = step!(model, agent_step!, 500, data_to_collect);

data

using UnicodePlots

N = sum(model.properties[:Ns]); # Total initial populationx = data.step;

## log plot
dead = max.(log10.(N .- data[:, Symbol("length(status)")]),0);
p = UnicodePlots.lineplot(max.(log10.(data[:, Symbol("infected(status)")]),0), name = "infected");
UnicodePlots.lineplot!(p, dead, name = "dead");
UnicodePlots.lineplot!(p, max.(log10.(data[:, Symbol("recovered(status)")]),0), name = "recovered")


## non log plot

dead = N .- data[:, Symbol("length(status)")];
p = UnicodePlots.lineplot(data[:, Symbol("infected(status)")], name = "infected");
UnicodePlots.lineplot!(p, dead, name = "dead");
UnicodePlots.lineplot!(p, data[:, Symbol("recovered(status)")], name = "recovered")
