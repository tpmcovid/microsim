# Here we simulate a Trasmission Network, Individual Level Model.
# The grounding idea is to have a 1:k sampling of NZ popoluation
# based out of the geografic location of all cities with more than 2000 inhabitants.
#
# This can be adapted to local area simulations using local densities.

using Distances,
      Pathogen,
      Random,
      UnicodePlots,
      Statistics,
      LinearAlgebra,
      Distributions,
      Random

using DataFrames,
      CSV,
      Query
      
# we read in the cities for lat, long and pop

# Scaling factor (Npoint:pop = 1:k)
k = 100

# We read in population and geo coordinates of the cities
cities = CSV.read("../../Data/cities/NZ_Urban_Area_Latitude_Longitude.csv") |>
	 @select(:City, :Island, :Population, :Latitude_DD, :Longitude_DD) |>
	 @mutate(Npoint = Int(ceil(parse(Int,replace(_.Population, "," => ""))/k)))

N = sum(DataFrame(cities).Npoint)


# We create some cluster by spreading the individual locations
# around the Latitude and Longitude coordinate of the towns
# Noise on latitude
σₗ = 0.001
# Noise on longitude
σᵥ = σₗ

points = @from i in cities begin
	@select {latitude = rand(Normal(i.Latitude_DD, σₗ), i.Npoint),
	      longitude = rand(Normal(i.Longitude_DD, σᵥ), i.Npoint)}
	@collect 
end;
points = points |> DataFrame;

y = collect(Iterators.flatten(points.latitude));
x = collect(Iterators.flatten(points.longitude));


# each individual can have a specified risk factor
# (we can make this depend on the age distribution by town)
risk_distribution = rand(Gamma(), N)

points = DataFrame(x = x, y = y, risk = risk_distribution)

# We scatter plot the coordinates, it should roughly recall
# New Zealand or the geographic area we are considering.
using UnicodePlots
scatterplot(x, y)

# Contagion risks is given by a square matrix of distances
# So far I'm using flat euclidean distance, but this may be improved
# (for example distancing North and South Island, or distancing
# between regions)
# Calculate distances
dists = [euclidean([points[i, :x]; points[i, :y]], [points[j, :x]; points[j, :y]]) for i = 1:N, j = 1:N]

# if we are working on a multiprocessor environment we should exploit it:
# using Distributed




# Initialise the population for the simulation
pop = Population(points, dists)

# We need to define the functions for the epidemic

# spark function:
function _constant(params::Vector{Float64}, pop::Population, i::Int64)
    return params[1]
end

function _one(params::Vector{Float64}, pop::Population, i::Int64)
    return 1.0
end


function _linear(params::Vector{Float64}, pop::Population, i::Int64)
    return params[1] * pop.risks[i, :risk]
end

function _powerlaw(params::Vector{Float64}, pop::Population, i::Int64, k::Int64)
    β = params[1]
    d = 100 * pop.distances[k, i]
    return d^(-β)
end

rf = RiskFunctions{SIR}(_constant, # sparks function
                        _one, # susceptibility function
                        _powerlaw, # infectivity function
                        _one, # transmissability function
                        _constant) # removal function

rparams = RiskParameters{SIR}([1.], # sparks function parameter(s)
			      Float64[], # susceptibility function parameter(s)
			      [4.0], # infectivity function parameter(s)
			      Float64[], # transmissibility function parameter(s)
		              [1.]) #
# Length of simulation
Tmax = 4000.

# A random seeded population of infected, with I₀ infected inviduals
I₀ = 100
starting_states = shuffle(append!(fill(State_I, I₀), fill(State_S, N-I₀)))

# initialize simulation
newsim = Simulation(pop, starting_states, rf, rparams)

# Run it
simulate!(newsim, nmax=10000)

events = observe(sim, Uniform(0.5, 2.5), Uniform(0.5, 2.5))


