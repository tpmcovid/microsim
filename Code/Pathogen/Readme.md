# Transmission Network simulation, population at 1:k scale

Here we simulate a Trasmission Network, Individual Level Model.
The grounding idea is to have a 1:k sampling of NZ popoluation based out of the geografic location of all cities with more than 2000 inhabitants.
The transmission network is complete, but we can specify forbidden links.
This can be adapted to local area simulations using population densities (i.e., transmission within a town or a region).

We can do MCMC estimation of both the parameters and of the contagion network for transmission in case we have complete (maybe noisy) observation of cases.

SIR or SEIR are available, this example is SIR.